package Figures;

public class Circle extends Figure{
    @Override
    public void getName() {
        System.out.println("Тип Фигуры : Круг");
    }
    @Override
    public void getDiametr(int RADIUS) {
        int DIAMETR = 2 * RADIUS;
        System.out.println("Радиус : " + RADIUS);
        System.out.println("Диаметр равен: " + DIAMETR);
    }


}
