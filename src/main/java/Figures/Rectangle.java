package Figures;

public class Rectangle extends Figure {
    @Override
    public void getName() {
        System.out.println("Тип фигуры: Прямоугольник");
    }
    @Override
    public void getDiagonal(int a, int b) {
        final int kvadrat = 2;
        int sqrDiagonal = (int) Math.pow(a,kvadrat) + (int)Math.pow(b,kvadrat);
        int diagonal = (int)Math.sqrt(sqrDiagonal);
        System.out.println("Диагональ прямоугольника равна : " + diagonal);
    }

    @Override
    public void getStoron(int a, int b) {
        if(a>b ){
            System.out.println("Длинная сторона " + a + "\n" + "Короткая сторона " + b);
        }else System.out.println("Длинная сторона " + b + "\n" + "Короткая сторона " + a);
    }
    public void getDiametr(int RADIUS){
        int diameter = RADIUS*2;
        System.out.println("Диаметр равен: " + diameter);
    }


}
